;;; r-ide-layer.el -*- lexical-binding: t; -*-
;; Package-Requires: ((emacs "26.1") (ess) (eglot))


;; TODO: set-up forge
;; TODO: set-up


;;; Dependencies
(require 'ess)
(straight-use-package '(radian-vterm :type git :host gitlab :repo "oryp6/editors_set-up/radian-vterm"))
(require 'eglot)

;;; Functions
(defun launch-radian-vterm-repl ()
  (interactive)
  (require 'radian-vterm)
  (radian-vterm-repl))

;;; Hooks
(add-hook 'ess-r-mode-hook 'radian-vterm-mode)
(add-hook 'org-mode-hook (lambda ()
			   (add-to-list 'org-babel-load-languages '(R . t))
			   (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)))
(add-hook 'ess-r-mode-hook #'eglot-ensure)

;;; Provide
(provide 'r-ide-layer)

;;; r-ide-layer.el ends here
